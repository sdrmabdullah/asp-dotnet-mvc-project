﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CustomerDataApi.Migrations
{
    /// <inheritdoc />
    public partial class updateDb4 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "registered",
                table: "Customers",
                type: "TEXT",
                nullable: false,
                defaultValue: "");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "registered",
                table: "Customers");
        }
    }
}
