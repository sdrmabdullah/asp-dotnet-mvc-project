﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CustomerDataApi.Migrations
{
    /// <inheritdoc />
    public partial class updateDbSchema : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Customers_Addresses_addressid",
                table: "Customers");

            migrationBuilder.RenameColumn(
                name: "addressid",
                table: "Customers",
                newName: "addressIdid");

            migrationBuilder.RenameIndex(
                name: "IX_Customers_addressid",
                table: "Customers",
                newName: "IX_Customers_addressIdid");

            migrationBuilder.AddForeignKey(
                name: "FK_Customers_Addresses_addressIdid",
                table: "Customers",
                column: "addressIdid",
                principalTable: "Addresses",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Customers_Addresses_addressIdid",
                table: "Customers");

            migrationBuilder.RenameColumn(
                name: "addressIdid",
                table: "Customers",
                newName: "addressid");

            migrationBuilder.RenameIndex(
                name: "IX_Customers_addressIdid",
                table: "Customers",
                newName: "IX_Customers_addressid");

            migrationBuilder.AddForeignKey(
                name: "FK_Customers_Addresses_addressid",
                table: "Customers",
                column: "addressid",
                principalTable: "Addresses",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
