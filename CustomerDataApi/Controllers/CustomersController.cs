﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CustomerDataApi.Data;
using CustomerDataApi.Models;
using CustomerDataApi.Controllers;

namespace CustomerDataApi.Controllers
{
    public class CustomersController : Controller
    {
        private ApplicationDbContext _context;

        public CustomersController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Customers
        public async Task<IActionResult> Index()
        {
              return View(await _context.Customers.ToListAsync());
        }

        // GET : Customers/SearchCustomer
        public async Task<IActionResult> SearchCustomer() {
            return View();
        }

        // POST: Customers/SearchResult/name
        [HttpPost]
        public async Task<IActionResult> SearchResult(string name)
        {
            Console.WriteLine("*****************************************************************");
            Console.WriteLine(name);
            Console.WriteLine("*****************************************************************");

            var customer = await _context.Customers.FindAsync(name);
            if (customer == null)
            {
                return NotFound();
            }
            return View();
        }

        // GET: Customers/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Customers/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("_id,index,age,eyeColor,name,gender,company,email,phone,about,registered,latitude,longitude")] Customer customer)
        {
            if (ModelState.IsValid)
            {
                _context.Add(customer);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(customer);
        }

        // GET: Customers/Edit
        public async Task<IActionResult> Edit()
        {
            return View(await _context.Customers.ToListAsync());
        }

        // GET: Customers/EditCustomer
        [HttpGet]
        public async Task<IActionResult> EditCustomer(string id)
        {
            if (id == null || _context.Customers == null)
            {
                return NotFound();
            }

            var customer = await _context.Customers
                .FirstOrDefaultAsync(m => m._id == id);
            if (customer == null)
            {
                return NotFound();
            }

            return View(customer);
        }

        // POST: Customers/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditCustomer(string _id, string name, [Bind("_id,index,age,eyeColor,name,gender,company,email,phone,about,registered,latitude,longitude")] Customer customer)
        {
            if (_id != customer._id)
            {
                Console.WriteLine("******************************** id not match *********************************");
                return NotFound();
            }

            else if (_id == customer._id)
            {
                try
                {
                    _context.Update(customer);
                    await _context.SaveChangesAsync();
                    Console.WriteLine("******************************** updated *********************************");
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CustomerExists(customer._id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        Console.WriteLine("******************************** error *********************************");
                        throw;
                    }
                }
                return RedirectToAction(nameof(EditCustomer));
            }
                
            return View(customer);
            
        }

        // GET: Customers/Edit
        public async Task<IActionResult> Distance()
        {
            return View(await _context.Customers.ToListAsync());
        }

        // POST: Customers/GetDistance/id
        [HttpGet]
        public async Task<IActionResult> GetDistance(string id) 
        {
            string distance;
            string customerName;

            if (id == null || _context.Customers == null)
            {
                return NotFound();
            }

            var customer = await _context.Customers.FindAsync(id);

            if (customer == null)
            {
                return NotFound();
            }
            else {
                string latitude = customer.latitude;
                string longitude = customer.longitude;
                customerName = customer.name;

                distance = CalcDistance.findDistance(latitude, longitude);
            }

            

            ViewBag.Distance = distance;
            ViewBag.CustomerName = customerName;
            return View();
        }

        // GET: Customers/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null || _context.Customers == null)
            {
                return NotFound();
            }

            var customer = await _context.Customers
                .FirstOrDefaultAsync(m => m._id == id);
            if (customer == null)
            {
                return NotFound();
            }

            return View(customer);
        }

        // POST: Customers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            if (_context.Customers == null)
            {
                return Problem("Entity set 'ApplicationDbContext.Customers'  is null.");
            }
            var customer = await _context.Customers.FindAsync(id);
            if (customer != null)
            {
                _context.Customers.Remove(customer);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CustomerExists(string id)
        {
          return _context.Customers.Any(e => e._id == id);
        }
    }
}
