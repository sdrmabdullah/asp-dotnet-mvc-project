﻿using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace CustomerDataApi.Models
{
    public class Customer
    {
        [Key, Required]
        public string _id { get; set; }
        [AllowNull]
        public int index { get; set; }
        [AllowNull]
        public int age { get; set; }
        [AllowNull]
        public string eyeColor { get; set; }
        [Required]
        public string name { get; set; }
        [AllowNull]
        public string gender { get; set; }
        [AllowNull]
        public string company { get; set; }
        [Required]
        public string email { get; set; }
        [Required]
        public string phone { get; set; }
        [AllowNull]
        public Address address { get; set; }
        [AllowNull]
        public string about { get; set; }
        [AllowNull]
        public string registered { get; set; }
        [AllowNull]
        public string latitude { get; set; }
        [AllowNull]
        public string longitude { get; set; }
    }

    public class Address {
        [Key]
        public int id { get; set; }
        [AllowNull]
        public int number { get; set; }
        [AllowNull]
        public string street { get; set; }
        [AllowNull]
        public string city { get; set; }
        [AllowNull]
        public string state { get; set; }
        [AllowNull]
        public int zipCode { get; set; }
    }
}
