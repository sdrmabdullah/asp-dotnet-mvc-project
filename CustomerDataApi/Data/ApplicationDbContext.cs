﻿using CustomerDataApi.Models;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace CustomerDataApi.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> option) : base(option)
        {
            InitializeData();
        }

        // customers table
        public DbSet<Customer> Customers { get; set; }

        // addresses table
        public DbSet<Address> Addresses { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Address>().Property(c => c.id).ValueGeneratedOnAdd();
        }

        public void InitializeData()
        {
            try
            {
                if (!Customers.Any())
                {
                    var json = File.ReadAllText("UserData.json");
                    var customers = JsonConvert.DeserializeObject<Customer[]>(json);

                    Customers.AddRange(customers);
                    SaveChanges();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
